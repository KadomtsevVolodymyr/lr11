﻿#include <iostream>
#include <iomanip>
#include "windows.h"
#include "time.h"

using namespace std;

int main()
{
    SetConsoleOutputCP(1251);
    SetConsoleCP(1251);
    double mass[5][5];
	cout << "Матриця: " << endl;
	for (int i = 0; i <= 4; i++)
	{
		for (int j = 0; j <= 4; j++)
		{
			mass[i][j] = rand() % 21 - 10;
			cout << setw(4) << setprecision(3) << mass[i][j];
		};
		cout << endl;
	};
	double sum = 0;
	int k = 0;
	for (int i = 0; i < 5; i++) // головна діагональ
	{
		sum += mass[i][i];
		k++;
	}
	double sum2 = 0;
	int j;
	for (int i = 0, j = 4; i < 5; i++) // друга діагональ
	{
		sum2 += mass[i][j];
		j--;
		k++;
	}
	double res_diagon = (sum + sum2) / k;
	cout << "Середнє значення діагональних елементів матриці = " << res_diagon << endl;
	for (int j = 0; j <= 4; j += 2)
	{
		for(int i = 0; i <= 4; i++)
		{
			mass[i][j] = mass[i][j] / res_diagon;
		}
	}
	cout << "Перетвоерна матриця за правилом: непарні стовпці розділити"<< endl <<"на середнє значення діагональних елементів матриці, а парні залишити без зміни. " << endl;
	for (int i = 0; i <= 4; i++)
	{
		for (int j = 0; j <= 4; j++)
		{
			cout << setw(12) << setprecision(3) << mass[i][j];
		};
		cout << endl;
	};
}
